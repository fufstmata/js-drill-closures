const invokeGenerator = require('../limitFunctionCallCount.cjs');
const assert = require('assert');

// Test callback function definition
// Easy to understand difference in when argument is ... and not ...
// If ..., then all arguments are accepted, if only args, then ONLY ONE argument is accepted and rest are ignored
// JavaScript ignores arguments beyond the limit set by function definition (always recall that)
function testing(args) {
    // console.log(args);
    return args;
}

const arr = [1,2,3,4]

const test = invokeGenerator(testing, 3);



// This works for both cases when callBack function is returning or not returning any value
// In the case of return we get out argument list and no return gives undefined, which also works for testing
let a = test(arr);
assert.deepStrictEqual(a,[1,2,3,4]);
console.log(a);
let b = test(a);
console.log(b);
let c = test(b);
console.log(c);
let d = test();
console.log(d)

// Nice demonstration of error and how it is fetched
// Don't forget about "throw" usage in main function
try {
    const test2 = invokeGenerator(testing);
} catch(errorMessage) {
    console.error(errorMessage);
}
