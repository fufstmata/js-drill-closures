// For cache testing, we care about the result retrieved from cache object if an argument that was previously found is detected
// The above premise is what becomes the basis for all our testing and parameter settings

const cacheGenerator = require('../cacheFunction.cjs');
const assert = require('assert');

function testing(args) {
    
    return args.map( current => -1*current);
}
const arr = [1,2,3,4];
const test = cacheGenerator(testing);



a = test(arr)
console.log(a)
b = test(a)
console.log(b)
c = test(b)
assert.deepStrictEqual(c,[-1,-2,-3,-4]);


// Nice demonstration of error and how it is fetched
// Don't forget about "throw" usage in main function
try {
    const test2 = cacheGenerator();
} catch(errorMessage) {
    console.error(errorMessage);
}



