const objectGenerator = require('../counterFactory.cjs');
const assert = require('assert');

const testObject = objectGenerator();

// Function testing

testObject.increment();

assert.strictEqual(testObject.counter, 1);

testObject.decrement();

assert.strictEqual(testObject.counter, 0);
