// Still need to see any limits placed or checks needed for testing purpose


function counterFactory() {

    // Declaring objects which have methods declared inside them with closure scope to update object values
    const obj = {
        counter : 0,

        // This and decrement method are good example of how we can access and update variables stored in objects
        // 
        increment: function() {
            this.counter += 1;
            return this.counter;
        },

        decrement: function() {
            this.counter -= 1;
            return this.counter;
        }
    }

    return(obj);

}

module.exports = counterFactory;


