// Incase you forget about how spread operator works, refer to demoSpreadOperator code in miscellaneous folder

function limitFunctionCallCount(callBack, recursiveCount) {


    // This argument insufficiency check is pretty neat
    // See how this works in conjunction with try-catch in test file.
    if (arguments.length < 2) {
        throw new Error('Insufficient parameters');
    } else {


        // Ask about usage of this counter object from a realistic project standpoint
        // Recall limit on failed login attempts scenario
        const functionCounter = {
            counter: 0
        }

        if (Number.isFinite(recursiveCount) && recursiveCount > 0) {

            // Using ... here is necessary since we will be passing argument to variable that fetches function definition of returning that invokes our callback
            function returning(...argumentList) {
                if (functionCounter.counter >= recursiveCount) {
                    return null;
                } else {
                    functionCounter.counter += 1;
                    // Line 21 IS VERY IMPORTANT
                    // Previously I was just doing callBack(...argumentList), but that is not always the case.
                    // Some callbacks can also return value.
                    // This line works since our invoked function in testing has original definition with all necessary arguments and data
                    // i.e. returning function definition, counter object and its attribute, and argument list
                    // Everytime we call our original variable which has the definition stored, we update all necessary details.
                    // Refer back with test function to see how this dance works (will help understand)
                    return callBack(...argumentList);
                }
            }

            return returning;
        } else {
            return null;
        }
    }




}

module.exports = limitFunctionCallCount;