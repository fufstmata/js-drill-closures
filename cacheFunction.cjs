// This problem can only be understood if you know how scoping works and how limitFunctionCallCount was executed
// along with spread operator and how the returns are work in function execution context
// Be mindful of how ... is used in this code


function cacheFunction(callBack) {


    // Again use of throw to fetch error. See how test file is using this with try and catch
    // try catch similar to try except in Python
    if (arguments.length < 1) {
        throw new Error('Empty parameter list. Provide callback function');
    } else {


        const cache = {
            resultList: {}
        }

        function returning(...args) {

            // This is basically checking for input callBack(...args) is present as a key by checking if it returns undefined or not in the cached object
            // If it doesn't then we will add the result and key value pair into the cache object
            // JSON.stringify is a big help instead of using .toString()
            // Trignometric functions and DSP filter functions are exceptions to this since in that case
            let currentArg = JSON.stringify([...args])
            if (cache.resultList[currentArg]) {

                return cache.resultList[currentArg];

            } else {

                let newValue = callBack(...args);
                cache.resultList[currentArg] = newValue;

                return newValue;

            }
        }

        return returning;
    }


}

module.exports = cacheFunction;